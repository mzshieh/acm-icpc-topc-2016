#!/usr/bin/env python3
import sys, os, json

if not os.path.isdir('workspace/'+sys.argv[1]):
    print('You need to create workspace/'+sys.argv[1],'as a directory.')
    sys.exit(0)

name = sys.argv[1]
meta = json.loads(open('meta.json','rt').read())
problemset = json.loads(open('problemset.json','rt').read())

if problemset['title'].get(name) != None:
    meta['basic']['title'] = problemset['title'].get(name)

if problemset['output_limit'].get(name) != None:
    meta['testdata'][0]['output_limit'] = problemset['output_limit'][name]

if problemset['time_limit'].get(name) != None:
    meta['testdata'][0]['time_limit'] = problemset['time_limit'][name]

if problemset['memory_limit'].get(name) != None:
    meta['testdata'][0]['memory_limit'] = problemset['memory_limit'][name]

if problemset['verdict'].get(name) != None:
    meta['verdict'] = problemset['verdict'][name]
else:
    del meta['verdict']

meta['basic']['file'] = 'P'+name+'.pdf'
meta['testdata'][0]['input'] = 'P'+name+'.in.txt'
meta['testdata'][0]['output'] = 'P'+name+'.out.txt'

print(json.dumps(meta,sort_keys=True,indent=4))
