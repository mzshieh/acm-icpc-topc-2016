#!/bin/bash
for i in A B C D E F G H I
do
	./each.py $i > workspace/$i/meta.json
	cd workspace/$i
	cp ../../../latex/P$i/P$i.pdf .
	cp ../../../testdata/P$i.* .
	cp ../../../validator/V$i* .
	zip ../../zip/$i.zip *
	cd ../..
done
