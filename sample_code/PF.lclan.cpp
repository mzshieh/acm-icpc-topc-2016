#include<iostream>
#include<vector>
#include<string>
#include<set>
#include<bitset>
#define shift 2200
using namespace std;
set<int> mph[shift*2];
set<int> mpv[shift*2];
bitset<shift*2> mapp[shift*2];
int minx,miny,maxx,maxy;
vector<pair<int,int> > v;
void dfs()
{
    int x,y;
    pair<int,int> tmp;
    int i=0;
    while(v.size()!=0)
    {
        tmp = v[v.size()-1];
        v.pop_back();
        x=tmp.first;
        y=tmp.second;
        mapp[x][y]=true;
        if(x>minx)
        {
            if(mpv[y].find(x)==mpv[y].end() && mapp[x-1][y]==false)
            {
                v.push_back(make_pair(x-1,y));
            }
        }
        if(x<maxx)
        {
            if(mpv[y].find(x+1)==mpv[y].end() && mapp[x+1][y]==false)
            {
                v.push_back(make_pair(x+1,y));
            }
        }
        if(y>miny)
        {
            if(mph[x].find(y)==mph[x].end() && mapp[x][y-1]==false)
            {
                v.push_back(make_pair(x,y-1));
            }
        }
        if(y<maxy)
        {
            if(mph[x].find(y+1)==mph[x].end() && mapp[x][y+1]==false)
            {
                v.push_back(make_pair(x,y+1));
            }
        }
    }

}
int main()
{
    int i,j,k,T;
    string s;
    cin>>T;
    while(T--)
    {
        for(i=0;i<shift*2;i++)
        {
            mph[i].clear();
            mpv[i].clear();
            mapp[i].reset();
        }
        cin>>s;
        int x,y;
        minx=miny=maxx=maxy=x=y=shift;
        for(i=0;i<s.size();i++)
        {
            if(s[i]=='R')
            {
                mph[x].insert(y);
                x++;
            }else if(s[i]=='L')
            {
                mph[x-1].insert(y);
                x--;
            }else if(s[i]=='U')
            {
                mpv[y].insert(x);
                y++;
            }else
            {
                mpv[y-1].insert(x);
                y--;
            }
            if(minx>x-10)
            {
                minx = x-10;
            }
            if(maxx<x+10)
            {
                maxx=x+10;
            }
            if(miny>y-10)
            {
                miny=y-10;
            }
            if(maxy<y+10)
            {
                maxy=y+10;
            }
        }
        int ans=0;
        for(i=minx;i<=maxx;i++)
        {
            for(j=miny;j<maxy;j++)
            {
                if(mapp[i][j]==false)
                {
                    v.clear();
                    v.push_back(make_pair(i,j));
                    ans++;
                    dfs();
                }
            }
        }
        cout<<ans<<endl;
    }
}

