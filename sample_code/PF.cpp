#include<bits/stdc++.h>

using namespace std;

struct edge{
    int coor[4]; // {x,X,y,Y}
    edge(int _x, int _y, int _X, int _Y)
    {
        coor[0] = min(_x,_X); // x
        coor[1] = max(_x,_X); // X
        coor[2] = min(_y,_Y); // y
        coor[3] = max(_y,_Y); // Y
    }
    bool operator<(const edge &rhs) const
    {
        for(int i = 0; i < 4; i++)
        {
            if(this->coor[i] < rhs.coor[i]) return true;
            if(this->coor[i] > rhs.coor[i]) return false;
        }
        return false;        
    }
};

set<pair<int,int> > V;
set<edge> E;

char str[65536];

void solve()
{
    V.clear();
    E.clear();
    scanf("%s",str);
    int x, y, nx, ny;
    x = y = 0;
    V.insert(make_pair(x,y));
    for(char *ptr = str; *ptr; ptr++)
    {
        nx = x;
        ny = y;
        if (*ptr == 'U') ny++;
        else if(*ptr == 'D') ny--;
        else if(*ptr == 'L') nx--;
        else if(*ptr == 'R') nx++;
        V.insert(make_pair(nx, ny));
        E.insert(edge(x,y,nx,ny));
        x = nx;
        y = ny;
    }
    printf("%ld\n",2+E.size()-V.size());
    // fprintf(stderr,"E: %ld, V: %ld\n",E.size(), V.size());
}

int main()
{
    int ncases;
    scanf("%d",&ncases);
    for(int i = 1; i <= ncases; i++)
        solve();
    return 0;
}