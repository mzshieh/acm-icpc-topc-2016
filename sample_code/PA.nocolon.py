#!/usr/bin/env python3
import sys

def run(states, tape):
    pos = 10
    state = 0
    for _ in range(10):
#        print(_,state,tape[pos],file=sys.stderr)
#        print(''.join((str(__) for __ in tape)),file=sys.stderr)
        tran = states[state][tape[pos]]
        tape[pos] = tran['wb']
        state = tran['next']
        pos = pos + tran['dir']
        if state == len(states):
            return True
    return False

def solve():
    n = int(input())
    states = []
    for i in range(1,n):
        state = input().lstrip('(').rstrip(')').split(') (')
        trans = []
        for tok in state:
            _ = tok.split(',')
            tran = {'next': int(_[0].strip())-1, 
                    'dir': int(_[1].strip()), 
                    'wb': int(_[2].strip())}
            trans.append(tran)
        states.append(trans)
    # print(states,file=sys.stderr)
    q = int(input())
    for i in range(q):
        tape = [2]*10 + list(map(int,input().split(' ')[1:])) + [2]*20
        if n == 1 or run(states, tape):
            print('yes')
        else:
            print('no')

num_cases = int(input())
for i in range(num_cases):
    print('Machine #%d' % (i+1))
    solve()
