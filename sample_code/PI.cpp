#include<bits/stdc++.h>
#define N 50010
using namespace std;
int ca,cb;
struct treap{
	treap *l,*r;
	long long val,add[2];
	int pos,apos,pri,sz;
	treap(){}
	treap(int _key, int _pos):l(NULL),r(NULL),pri(rand()),val((1LL*_pos*_pos+1LL*ca*_pos+cb)*_key),apos(0),pos(_pos),sz(1){add[0]=add[1]=0;}
}pool[N];
long long ans[N];
int p[N];
inline int size(treap *t){
	return t?t->sz:0;
}
inline void push(treap *t){
	if(t->l){
		t->l->add[0]+=t->add[0]+t->add[1]*t->l->apos;
		t->l->add[1]+=t->add[1];
		t->l->apos+=t->apos;
	}
	if(t->r){
		t->r->add[0]+=t->add[0]+t->add[1]*t->r->apos;
		t->r->add[1]+=t->add[1];
		t->r->apos+=t->apos;
	}
	t->val+=t->add[1]*t->pos+t->add[0];
	t->pos+=t->apos;
	t->add[1]=t->add[0]=0;
	t->apos=0;
}
inline void pull(treap *t){
	t->sz=1+size(t->l)+size(t->r);
}
inline long long wt(int pos){
	return 1LL*pos*pos+pos*ca+cb;
}
treap *merge(treap *a,treap *b){
	if(!a||!b) return a?a:b;
	if(a->pri>b->pri){
		push(a);
		a->r=merge(a->r,b);
		pull(a);
		return a;
	}
	else{
		push(b);
		b->l=merge(a,b->l);
		pull(b);
		return b;
	}
}
void split(treap *t, treap *&a,treap *&b, int th){
	if(!t){
		a=b=NULL;
		return;
	}
	push(t);
	if(t->val>th*wt(t->pos)){
		a=t;
		split(t->r,a->r,b,th);
	}
	else{
		b=t;
		split(t->l,a,b->l,th);
	}
	pull(t);
}
long long trav(treap *t,long long pre){
	push(t);
	if(t->l) pre=trav(t->l,pre);
	pre+=t->val;
	ans[t->pos]=pre;
	if(t->r) pre=trav(t->r,pre);
	return pre;
}
int main(){
	int T,n;
	scanf("%d",&T);
	assert(T<=20);
	while(T--){
		treap *rt=NULL,*a,*b;
		scanf("%d",&n);
		assert(n<=50000);
		for(int i=0;i<n;i++){
			scanf("%d",&p[i]);
			assert(p[i]<=50000&&p[i]>=-50000);
		}
		scanf("%d%d",&ca,&cb);
		assert(ca*ca>=cb*4);
		assert(ca>=0&&ca<=100&&cb>=0&&cb<=100);
		for(int i=0;i<n;i++){
			split(rt,a,b,p[i]);
			pool[i] = treap(p[i],size(a)+1);
			if(b){
				b->add[1]+=2*p[i];
				b->add[0]+=1LL*p[i]*(2*b->apos+ca+1);
				b->apos++;
			}
			rt = merge(a,merge(pool+i,b));
		}
		trav(rt,0);
		for(int i=1;i<=n;i++) printf("%lld%c",ans[i],i==n?'\n':' ');
	}
	return 0;
}
