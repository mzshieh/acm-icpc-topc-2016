#include<bits/stdc++.h>
using namespace std;
const int N=26*26*26*26;
const int p[8]={2,3,5,7,11,13,17,19};
char st[10];
int num[N];
int val[N+1];
const int p26[4]={1,26,26*26,26*26*26};
int cal(int now,int l,int pre,int s,int q){
	if(now==l) return pre==s;
	int x=p[now],res=0,tmp;
	for(int i=0;i<26;i++){
		if(tmp=cal(now+1,l,1LL*pre*x%q,s,q)) st[now]='A'+i,res+=tmp;
		x=1LL*x*p[now]%q;
	}
	return res;
}
bool cmp(int a, int b){
	return val[a]<val[b];
}
long long solve(int now, int l, int pre, int q){
	if(now==l){
		val[N]=pre;
		int x=lower_bound(num,num+N,N,cmp)-num,y=upper_bound(num,num+N,N,cmp)-num;
		if(y-x==1){
			int tmp=num[x];
			for(int j=0;j<4;j++){
				st[j]='A'+tmp%26;
				tmp/=26;
			}
		}
		return y-x;
	}
	long long x=p[now],res=0,tmp;
	for(int i=0;i<26;i++){
		if(tmp=solve(now+1,l,pre*x%q,q)) st[now]='A'+i,res+=tmp;
		x=x*p[now]%q;
	}
	return res;
}
int inv(int s,int q){
	int y=q-2,r=1;
	while(y){
		if(y&1) r=1LL*r*s%q;
		s=1LL*s*s%q;
		y>>=1;
	}
	return r;
}
int main(){
	int T,q,l,s,sz;
	bool in;
	int ip[4];
	//scanf("%d",&T);
	//while(T--){
	while(1){
		scanf("%d%d%d",&l,&s,&q);
		if(q==0) break;
		st[l]=0;
		in=false;
		for(int i=0;i<l;i++){
			if(p[i]==q) in=true;
		}
		if(in){
			if(s) puts("not a word");
			else puts("ambiguous");
		}
		else if(s==0) puts("not a word");
		else if(l<=4){
			int tmp=cal(0,l,1,s,q);
			if(tmp==0) puts("not a word");
			else if(tmp>1) puts("ambiguous");
			else puts(st);
		}
		else{
			for(int i=0;i<4;i++){
				long long tmp=1;
				while(tmp%p[i]) tmp+=q;
				ip[i]=tmp/p[i];
			}
			long long pi=1,pj=1,pk=1,pl=1;
			for(int i=0;i<26;i++){
				pi=pi*ip[0]%q;
				pj=pk=pl=1;
				for(int j=0;j<26;j++){
					pj=pj*ip[1]%q;
					pk=pl=1;
					for(int k=0;k<26;k++){
						pk=pk*ip[2]%q;
						pl=1;
						for(int l=0;l<26;l++){
							pl=pl*ip[3]%q;
							val[i+j*p26[1]+k*p26[2]+l*p26[3]]=pi*pj%q*pk%q*pl%q;
						}
					}
				}
			}
			for(int i=0;i<N;i++) num[i]=i;
			sort(num,num+N,cmp);
			long long tmp=solve(4,l,inv(s,q),q);
			if(tmp==1) puts(st);
			else if(tmp) puts("ambiguous");
			else puts("not a word");
		}
	}
	return 0;
}