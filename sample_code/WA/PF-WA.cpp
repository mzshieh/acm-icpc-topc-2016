#include<bits/stdc++.h>

using namespace std;

typedef pair<int,int> pii;
map<pii,set<char> > V;

char str[70000];

void solve()
{
    V.clear();
    scanf("%s",str);
    int x, y, nx, ny;
    x = y = 0;
    for(char *ptr = str; *ptr; ptr++)
    {
        nx = x;
        ny = y;
        char inv = '?';
        if (*ptr == 'U') ny++, inv='D';
        else if(*ptr == 'D') ny--, inv='U';
        else if(*ptr == 'L') nx--, inv='R';
        else if(*ptr == 'R') nx++, inv='L';
        V[make_pair(x, y)].insert(*ptr);
        V[make_pair(nx,ny)].insert(inv);
        x = nx;
        y = ny;
    }
    priority_queue<pair<int,pii>> pq;
    for(auto v: V)
    {
        pq.push(make_pair(-(int)v.second.size(),v.first));
//fprintf(stderr,"deg(%d,%d)=%d: ",v.first.first,v.first.second,(int)v.second.size());
//for(auto c: v.second) fprintf(stderr,"%c",c);
//fprintf(stderr,"\n");
    }
    int ans = 1;
    set<pii> done;
    while(!pq.empty())
    {
        int key = -pq.top().first;
        pii pos=pq.top().second;
        
        pq.pop();
        if(done.count(pos))continue;
        fprintf(stderr,"%d deg(%d,%d)=%d\n",key,pos.first,pos.second,(int)V[pos].size());
        if(V[pos].size()>1) ans++;
        for(auto dir: V[pos])
        {
            pii new_pos = pos;
            char inv = '?';
            if(dir=='U') new_pos.second++, inv='D';
            else if(dir=='D') new_pos.second--, inv='U';
            else if(dir=='L') new_pos.first--, inv='R';
            else if(dir=='R') new_pos.first++, inv='L';
            V[new_pos].erase(inv);
            pq.push(make_pair(-(int)V[new_pos].size(),new_pos));
        }
        V[pos].clear();
        done.insert(pos);
    }
    printf("%d\n",ans);
    //printf("%ld\n",2+E.size()-V.size());
    // fprintf(stderr,"E: %ld, V: %ld\n",E.size(), V.size());
}

int main()
{
    int ncases;
    scanf("%d",&ncases);
    for(int i = 1; i <= ncases; i++)
        solve();
    return 0;
}
