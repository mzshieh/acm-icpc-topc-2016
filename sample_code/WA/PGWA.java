import java.io.*;
import java.lang.*;
import java.math.*;
import java.util.*;

class PGWA{
    public static void main(String args[]) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        long longBase[] = {2L,3L,5L,7L,11L,13L,17L,19L};
        long expectedSize[] = {1L,26L,676L,17576L,456976L,456976L,456976L,456976L,456976L};
        long prefix[][][][] = new long[26][26][26][26];
        long suffix[][][][] = new long[26][26][26][26];
        mainLoop:
        while(true)
        {
            int len = sc.nextInt();
            long cipher = sc.nextLong();
            long prime = sc.nextLong();
            BigInteger bigPrime = BigInteger.valueOf(prime);
            if(prime==0L) break;
            for(int i = 0; i < len; i++)
                if(prime == longBase[i])
                {
                    System.out.println(cipher==0L?"ambiguous":"not a word");
                    continue mainLoop;
                }
            long base[] = new long[8];
            for(int i = 0; i < len; i++)
                base[i] = longBase[i];
            for(int i = len; i < 8; i++)
                base[i] = 1L;
                
            long invBase[] = new long[8];
            for(int i = 0; i < 8; i++)
                invBase[i] = BigInteger.valueOf(base[i]).modInverse(bigPrime).longValue();

            prefix[0][0][0][0]=base[0]*base[1]*base[2]*base[3]%prime;
            for(int w = 1; w < 26; w++)
                prefix[w][0][0][0]=prefix[w-1][0][0][0]*base[0]%prime;
            for(int w = 0; w < 26; w++)
                for(int x = 1; x < 26; x++)
                    prefix[w][x][0][0]=prefix[w][x-1][0][0]*base[1]%prime;
            for(int w = 0; w < 26; w++)
                for(int x = 0; x < 26; x++)
                    for(int y = 1; y < 26; y++)
                        prefix[w][x][y][0]=prefix[w][x][y-1][0]*base[2]%prime;
            for(int w = 0; w < 26; w++)
                for(int x = 0; x < 26; x++)
                    for(int y = 0; y < 26; y++)
                        for(int z = 1; z < 26; z++)
                            prefix[w][x][y][z]=prefix[w][x][y][z-1]*base[3]%prime;
                            
            HashSet<Long> prefixes = new HashSet<Long>();
            for(int w = 0; w < 26; w++)
                for(int x = 0; x < 26; x++)
                    for(int y = 0; y < 26; y++)
                        for(int z = 0; z < 26; z++)
                            prefixes.add(prefix[w][x][y][z]);

            int cnt = (len <= 4 && expectedSize[len] != prefixes.size() && prefixes.contains(cipher))?2:0;
            long sufPart = 0;
            char ans[] = new char[8];
            if(len>4 || prefixes.contains(cipher))
            {
                suffix[0][0][0][0]=BigInteger.valueOf(base[4]*base[5]*base[6]*base[7]).modInverse(bigPrime).longValue();
                for(int w = 1; w < 26; w++)
                    suffix[w][0][0][0]=suffix[w-1][0][0][0]*invBase[4]%prime;
                for(int w = 0; w < 26; w++)
                    for(int x = 1; x < 26; x++)
                        suffix[w][x][0][0]=suffix[w][x-1][0][0]*invBase[5]%prime;
                for(int w = 0; w < 26; w++)
                    for(int x = 0; x < 26; x++)
                        for(int y = 1; y < 26; y++)
                            suffix[w][x][y][0]=suffix[w][x][y-1][0]*invBase[6]%prime;
                for(int w = 0; w < 26; w++)
                    for(int x = 0; x < 26; x++)
                        for(int y = 0; y < 26; y++)
                            for(int z = 1; z < 26; z++)
                                suffix[w][x][y][z]=suffix[w][x][y][z-1]*invBase[7]%prime;
                for(int w = (len<5)?25:0; w < 26; w++)
                    for(int x = (len<6)?25:0; x < 26; x++)
                        for(int y = (len<7)?25:0; y < 26; y++)
                            for(int z = (len<8)?25:0; z < 26; z++)
                            {
                                long target = suffix[w][x][y][z]*cipher%prime;
                                if(!prefixes.contains(target)) continue;
                                if(cnt++>0)
                                {
                                    System.out.println("ambiguous");
                                    continue mainLoop;
                                }
                                else
                                {
                                    ans[4]=(char)('A'+w);
                                    ans[5]=(char)('A'+x);
                                    ans[6]=(char)('A'+y);
                                    ans[7]=(char)('A'+z);
                                    sufPart = target;
                                }
                            }
            }
            if(cnt==0)
            {
                System.out.println("not a word");
            }
            else if(cnt>1)
                System.out.println("ambiguous");
            else
            {
                prePartLoop:
                for(int w = 0; w < 26; w++)
                    for(int x = 0; x < 26; x++)
                        for(int y = 0; y < 26; y++)
                            for(int z = 0; z < 26; z++)
                                if(prefix[w][x][y][z]==sufPart)
                                {
                                    ans[0]=(char)('A'+w);
                                    ans[1]=(char)('A'+x);
                                    ans[2]=(char)('A'+y);
                                    ans[3]=(char)('A'+z);
                                    break prePartLoop;
                                }
                System.out.println(new String(ans).substring(0,len));
            }
        }
    }
}
