#!/usr/bin/env python3
result={}
n=None

def win(determined,assignment,is_cook):
    global n, result
    if determined == (1<<n)-1:
        if is_cook:
            return result[(determined,assignment)]
        else:
            return not result[(determined,assignment)]
    ret = result.get((determined,assignment))
    if ret != None:
        return ret
    for i in range(n):
        if ((1<<i) & determined) == 0:
            if not win(determined | (1<<i), assignment, not is_cook):
                result[(determined,assignment)]=True
                return True
            if not win(determined | (1<<i), assignment | (1<<i), not is_cook):
                result[(determined,assignment)]=True
                return True
    result[(determined,assignment)] = False
    return False

def check(formula,assignment):
    if n<=5:
        for var, val in zip('ABCDE',assignment):
            formula = formula.replace(var,str(val))
    else:
        for var, val in zip('FABCDEGHIJ',assignment):
            formula = formula.replace(var,str(val))
    return eval(formula)

def solve():
    global n, result
    result = {}
    n = int(input())
    formula = input()
    assignments = [()]
    for i in range(n):
        assignments = [(False,)+_ for _ in assignments]+[(True,)+_ for _ in assignments]
    for it in zip(range(1<<n),assignments):
        result[((1<<n)-1,it[0])] = check(formula,it[1])
    if win(0,0,True):
        print('Cook')
    else:
        print('Levin')

num_cases = int(input())
for _ in range(num_cases):
    solve()
