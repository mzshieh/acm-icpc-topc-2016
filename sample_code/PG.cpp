#include<bits/stdc++.h>

using namespace std;

long long inv(long long a, long long b)
{
    return 1LL<a ? b - inv(b%a,a)*b/a : 1LL;
}

long long prefixes[456976], suffixes[456976];
unordered_map<long long, int> pat;

void solve(int len, long long cipher, long long prime)
{
    long long base[8]={2LL,3LL,5LL,7LL,11LL,13LL,17LL,19LL};
    int expected_size[9]={1,26,676,17576,456976,456976,456976,456976,456976};
    for(int i = len; i < 8; i++)
        base[i] = 1LL;
    for(int i = 4; i < len; i++)
        base[i] = base[i]%prime?inv(base[i],prime):0;
    prefixes[0]=base[0]*base[1]*base[2]*base[3]%prime;
    int pos=1;
    for(int i = 0; i < 4 && i < len; i++)
        for(;pos<expected_size[i+1];pos++)
            prefixes[pos]=prefixes[pos-expected_size[i]]*base[i]%prime;

    for(int i = 0; i < expected_size[len]; i++)
    {
        if(pat.count(prefixes[i])==0)
            pat[prefixes[i]]=i;
        else pat[prefixes[i]]=-1;
    }

    if(len<=4)
    {
        if(pat.count(cipher)==0)
            puts("not a word");
        else if(pat[cipher]==-1)
            puts("ambiguous");
        else
        {
            int x = pat[cipher];
            for(int i = 0; i < len; i++, x/=26)
                printf("%c",x%26+'A');
            puts("");
        }
        return;
    }

    suffixes[0] = ((base[4]*base[5]%prime)*base[6]%prime)*base[7]%prime;
    pos = 1;
    for(int i = 0; i < 4 && i < len-4; i++)
        for(;pos<expected_size[i+1]; pos++)
            suffixes[pos]=suffixes[pos-expected_size[i]]*base[i+4]%prime;

    if(cipher==0LL && suffixes[0]==0LL)
    {
        puts("ambiguous");
        return;
    }
    
    int pre=-2, suf=-2; // -2: not found, -1: ambiguous
    for(int i = 0; i < expected_size[len-4] && pre!=-1; i++)
    {
        long long target = cipher*suffixes[i]%prime;
        if(pat.count(target))
        {
            if(pre>=0) pre=-1;
            else pre=pat[target], suf=i;
        }
    }
    if(pre==-2) puts("not a word");
    else if(pre==-1) puts("ambiguous");
    else
    {
        for(int i = 0; i < 4; i++, pre/=26)
            printf("%c",pre%26+'A');
        for(int i = 4; i < len; i++, suf/=26)
            printf("%c",suf%26+'A');
        puts("");
    }
}

int main()
{
    int len, cipher, prime;
    for(scanf("%d%d%d",&len, &cipher, &prime); len || cipher || prime; scanf("%d%d%d",&len, &cipher, &prime))
    {
        pat.clear();
        //pat.reserve(1048576);
        solve(len,cipher,prime);
    }
	return 0;
}