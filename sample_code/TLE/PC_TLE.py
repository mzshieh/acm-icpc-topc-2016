#!/usr/bin/env python3
result={}
var = []

def win(formula,is_cook):
    global result
    global determined
    if is_cook:
        key = 'cook: '+formula
    else:
        key = 'levin: '+formula 
    if len(determined) == len(var):
        if is_cook:
            return eval(formula.replace('true','True').replace('false','False'))
        else:
            return not eval(formula.replace('true','True').replace('false','False'))
    ret = result.get(key)
    if ret!=None:
        return ret
    for i in var:
        if i not in determined:
            # try set i
            determined.add(i)
            if not win(formula.replace(i,'true'),not is_cook) or not win(formula.replace(i,'false'),not is_cook):
                result[formula] = True
                determined.remove(i)
                return True
            determined.remove(i)
    result[key] = False
    return False

def solve():
    global var
    global result
    global determined
    result = {}
    n = int(input())
    formula = input()
    var = [_ for _ in 'ABCDEFGHIJ'[:n]]
    determined = set()
    if win(formula,True):
        print('Cook')
    else:
        print('Levin')

num_cases = int(input())
for _ in range(num_cases):
    solve()
