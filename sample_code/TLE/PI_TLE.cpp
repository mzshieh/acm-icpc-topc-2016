#include<bits/stdc++.h>
#define N 50010
using namespace std;
long long dp[N],r[N];
int p[N];
int main(){
	int T;
	scanf("%d",&T);
	while(T--){
		int n,a,b;
		scanf("%d",&n);
		dp[0]=0;
		for(int i=0;i<n;i++){
			scanf("%d",&p[i]);
		}
		scanf("%d%d",&a,&b);
		for(int i=0;i<n;i++){
			r[i+1]=1LL*(i+1)*(i+1)+a*(i+1)+b;
			dp[i+1]=dp[i]+r[i+1]*p[i];
			for(int j=i;j>0;j--){
				dp[j]=max(dp[j],dp[j-1]+r[j]*p[i]);
			}
		}
		for(int i=1;i<=n;i++) printf("%lld%c",dp[i],i==n?'\n':' ');
	}
	fprintf(stderr,"%.2f s\n",1.0*clock()/CLOCKS_PER_SEC);
	return 0;
}