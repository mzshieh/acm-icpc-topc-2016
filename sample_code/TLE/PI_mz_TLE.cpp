#include<bits/stdc++.h>

using namespace std;

int mul[50000];
long long best[50001];

void solve()
{
	int n, a, b;
	scanf("%d",&n);
	for(int i = 0; i < n; i++)
		scanf("%d",&mul[i]);
	scanf("%d%d",&a,&b);
	best[0] = 0;
	for(int i = 1; i <= n; i++)
		best[i] = -(0x7FFFFFFFFFFFFFFFLL);
	for(int i = 0; i < n; i++)
	{
		for(int j = i; j >= 0; j--)
		{
			long long k=j+1;
			long long w = (long long)mul[i]*(k*k+a*k+b);
			best[k]=max(best[k],best[j]+w);
		}
	}
	for(int i = 1; i <= n; i++)
		printf("%lld%c",best[i],i==n?'\n':' ');
}

int main()
{
	int ncases;
	scanf("%d",&ncases);
	while(ncases--)
		solve();
	return 0;
}
