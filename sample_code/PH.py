#!/usr/bin/env python3
import random, sys

def rl():
    return list(map(int,input().split()))

def solve():
    n = rl()[0]
    u = rl()
    v = rl()
    cnt = 0
    for i in range(n):
        if u[i] != v[i]:
            cnt = cnt + 1
    print(cnt)

num_cases = rl()[0]
for _ in range(num_cases):
    solve()
