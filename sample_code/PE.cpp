#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<vector>
using namespace std;
const int MAXN = 10000 + 10;
const int MAXM = 6000 + 10;
const int INF = 1023456789;
struct P{
    int x,y,i;
    P(){}
    P(int _x,int _y,int _i):x(_x),y(_y),i(_i){}
}T[MAXM],p[MAXN];
struct M{
    int u,v,w;
    M(){}
    M(int _u,int _v,int _w):u(_u),v(_v),w(_w){}
};
vector<M>e;
int n,par[MAXN];
int find(int x){
    return x==par[x]? x : par[x]=find(par[x]);
}
bool cmp1(P a,P b){
    if(a.x!=b.x)return a.x < b.x;
    return a.y < b.y;
}
bool cmp2(M a,M b){
    return a.w < b.w;
}
void query(int t,int w,int i){
    t+=3000;
    int mini=INF,cid=-1;
    while(t<MAXM){
        if(mini>T[t].x){
            mini=T[t].x;
            cid=T[t].i;
        }
        t+=(t&(-t));
    }
    if(cid!=-1)e.push_back(M(i,cid,mini-w));
}
void update(int t,int w,int i){
    t+=3000;
    while(t>0){
        if(T[t].x>w){
            T[t].x=w;
            T[t].i=i;
        }
        t-=(t&(-t));
    }
}
void make_graph(){
    sort(p,p+n,cmp1);
    for(int i=0;i<MAXM;i++)T[i].x=INF,T[i].i=-1;
    for(int i=n-1;i>=0;i--){
        query(p[i].y-p[i].x,p[i].y+p[i].x,p[i].i);
        update(p[i].y-p[i].x,p[i].y+p[i].x,p[i].i);
    }
}
int main(){
    int T;
    int ans;
    scanf("%d",&T);
    while(T--){
        scanf("%d",&n);
        e.clear();
        for(int i=0;i<n;i++)scanf("%d%d",&p[i].x,&p[i].y),p[i].i=i;

        make_graph();
        for(int i=0;i<n;i++)swap(p[i].x,p[i].y);
        make_graph();
        for(int i=0;i<n;i++)p[i].y=-p[i].y;
        make_graph();
        for(int i=0;i<n;i++)swap(p[i].x,p[i].y);
        make_graph();

        for(int i=0;i<n;i++)par[i]=i;
        sort(e.begin(),e.end(),cmp2);
        ans=0;
        for(int i=0;i<e.size();i++){
            int x=find(e[i].u),y=find(e[i].v);
            if(x!=y){
                n--;
                par[x]=y;
                ans+=e[i].w;
            }
            if(n==1)break;
        }
        printf("%d\n",ans*2);
    }
    return 0;
}
