#include<bits/stdc++.h>
using namespace std;
char st[1000];
char *tok[1000];
bitset<1024> win[1024];
bitset<1024> get_expr(int&);
bool ch[1024];
bitset<1024> get_factor(int &l){
	bitset<1024> tmp;
	if(*tok[l]=='('){
		tmp=get_expr(++l);
		++l;
	}
	else if(*tok[l]=='n'){
		tmp=~get_factor(++l);
	}
	else{
		int r=(1<<*tok[l]-'A');
		for(int i=0;i<1024;i++){
			if(i&r) tmp.set(i);
		}
		++l;
	}
	return tmp;
}
bitset<1024> get_term(int &l){
	bitset<1024> tmp;
	tmp.set();
	while(tok[l]&&*tok[l]!=')'&&*tok[l]!='o'){
		if(*tok[l]=='a') l++;
		tmp&=get_factor(l);
	}
	return tmp;
}
bitset<1024> get_expr(int &l){
	bitset<1024> tmp;
	while(tok[l]&&*tok[l]!=')'){
		if(*tok[l]=='o') l++;
		tmp|=get_term(l);
	}
	return tmp;
}
int main(){
	int T;
	scanf("%d",&T);
	while(T--){
		int n=0,m;
		scanf("%d",&m);
		gets(st);
		gets(st);
		tok[n]=strtok(st," ");
		while(tok[n]){
			tok[++n]=strtok(NULL," ");
		}
		int l=0;
		win[(1<<m)-1]=get_expr(l);
		ch[0]=false;
		for(int i=1;i<(1<<m);i++){
			ch[i]=!ch[i^(i&-i)];
			if(ch[i-1]) win[i-1].set();
			else win[i-1].reset();
		}
		for(int i=(1<<m)-1;i>0;i--){
			for(int j=i;;j=(j-1)&i){
				if(win[i][j]==ch[i]){
					for(int k=0;k<m;k++){
						if(i&(1<<k)){
							win[i^(1<<k)][j&~(1<<k)]=ch[i];
						}
					}
				}
				if(j==0) break;
			}
		}
		if(win[0][0]) puts("Cook");
		else puts("Levin");
	}
	return 0;
}
