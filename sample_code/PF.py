#!/usr/bin/env python3
def solve():
    walk = input()
    pos = (0,0)
    V = {pos}
    E = set()
    for _ in walk:
        if _ == 'U':
            new_pos = (pos[0],pos[1]+1)
        elif _ == 'D':
            new_pos = (pos[0],pos[1]-1)
        elif _ == 'L':
            new_pos = (pos[0]-1,pos[1])
        elif _ == 'R':
            new_pos = (pos[0]+1,pos[1])
        if pos < new_pos:
            E.add((pos,new_pos))
        else:
            E.add((new_pos,pos))
        pos = new_pos
        V.add(pos)
    print(2-len(V)+len(E))

num_tests = int(input())
for _ in range(num_tests):
    solve()
