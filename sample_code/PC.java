import java.io.*;
import java.util.*;

public class PC{

    static int[][] chart;
    static Node root;
    static int vars = 0;

    public static void main(String[] args){
        Scan scan = new Scan();
        int testcases = scan.nextInt();
        while(testcases-- != 0){
            vars = scan.nextInt();
            String s = scan.nextLine();
            StringTokenizer input = new StringTokenizer(s);
            //System.out.println("input : "+s);
            ArrayList<Integer> op = new ArrayList<Integer>();
            ArrayList<Node> stack = new ArrayList<Node>();
            buildTree(input, op, stack);
            root = stack.get(0);
            //root.print();
            chart = new int[1<<vars][1<<vars];
            for(int i=0;i<(1<<vars);i++) Arrays.fill(chart[i], -1);
            boolean result = cookPlay(0, 0);
            if(result) System.out.println("Cook");
            else System.out.println("Levin");
            //for(int i=0;i<1024;i++){
                //System.out.println(i+" : "+chart[1023][i]);
            //}
        }
    }

    static boolean cookPlay(int used, int value){
        if(chart[used][value] != -1) return chart[used][value] == 1;
        if(used == (1<<vars)-1){
            chart[used][value] = root.evaluate(value) ? 1 : 0;
            return chart[used][value] == 1;
        }
        boolean result = false;
        for(int i=0;i<vars;i++){
            if(((used>>i)&1) == 1) continue;
            used ^= (1<<i);
            if(result = levinPlay(used, value)) break;
            value ^= (1<<i);
            if(result = levinPlay(used, value)) break;
            value ^= (1<<i);
            used ^= (1<<i);
        }
        if(result){
            chart[used][value] = 1;
            return true;
        }else{
            chart[used][value] = 0;
            return false;
        }
    }

    static boolean levinPlay(int used, int value){
        if(chart[used][value] != -1) return chart[used][value] == 1;
        if(used == (1<<vars)-1){
            chart[used][value] = root.evaluate(value) ? 1 : 0;
            return chart[used][value] == 1;
        }
        boolean result = true;
        for(int i=0;i<vars;i++){
            if(((used>>i)&1) == 1) continue;
            used ^= (1<<i);
            if(!(result = cookPlay(used, value))) break;
            value ^= (1<<i);
            if(!(result = cookPlay(used, value))) break;
            value ^= (1<<i);
            used ^= (1<<i);
        }
        if(result){
            chart[used][value] = 1;
            return true;
        }else{
            chart[used][value] = 0;
            return false;
        }
    }

    static void buildTree(StringTokenizer input, ArrayList<Integer> op, ArrayList<Node> stack){
        // !:0 &:1 |:2 (:3
        while(input.hasMoreElements()){
            String s = input.nextToken();
            //System.out.println("reading : "+s);
            if(s.compareTo("(") == 0){
                op.add(3);
            }else if(s.compareTo(")") == 0){
                while(!op.isEmpty()){
                    int last = op.remove(op.size()-1);
                    if(last == 3) break;
                    push(last, stack);
                }
            }else if(s.compareTo("not") == 0){
                op.add(0);
            }else if(s.compareTo("and") == 0){
                while(!op.isEmpty()){
                    int last = op.get(op.size()-1);
                    if(last > 1) break;
                    op.remove(op.size()-1);
                    push(last, stack);
                }
                op.add(1);
            }else if(s.compareTo("or") == 0){
                while(!op.isEmpty()){
                    int last = op.get(op.size()-1);
                    if(last > 2) break;
                    op.remove(op.size()-1);
                    push(last, stack);
                }
                op.add(2);
            }else{
                stack.add(new Node(s.charAt(0)));
            }
        }
        while(op.size() != 0){
            push(op.remove(op.size()-1), stack);
        }
    }

    static void push(int id, ArrayList<Node> stack){
        Node now = new Node((char)('0'+id));
        now.right = stack.remove(stack.size()-1);
        if(id != 0) now.left = stack.remove(stack.size()-1);
        stack.add(now);
    }

    static class Node{

        char c;
        Node left, right;

        Node(char c){
            this.c = c;
            left = right = null;
        }

        boolean evaluate(int value){
            if(c >= '0' && c <= '2'){
                if(c == '0') return !right.evaluate(value);
                if(c == '1') return left.evaluate(value) && right.evaluate(value);
                return left.evaluate(value) || right.evaluate(value);
            }
            return ((value>>(c-'A')) & 1) == 1;
        }
        
        void print(){
            System.out.println(c);
            if(left == null) System.out.println("null");
            else left.print();
            if(right == null) System.out.println("null");
            else right.print();
        }

    }

}

class Scan{

    BufferedReader buffer;
    StringTokenizer tok;

    Scan(){
        buffer = new BufferedReader(new InputStreamReader(System.in));
    }

    boolean hasNext(){
        while(tok==null || !tok.hasMoreElements()){
            try{
                tok = new StringTokenizer(buffer.readLine());
            }catch(Exception e){
                return false;
            }
        }
        return true;
    }

    String next(){
        if(hasNext()) return tok.nextToken();
        return null;
    }

    String nextLine(){
        if(hasNext()) return tok.nextToken("\n");
        return null;
    }

    int nextInt(){
        return Integer.parseInt(next());
    }

}
