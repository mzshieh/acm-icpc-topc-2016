#!/usr/bin/env python3

def solve():
    line = 1
    intervals = []
    statement = input().strip(';')
    while statement != 'END':
        if statement.startswith('if'):
            intervals.append((int(statement.split('_')[-1]),-line))
        line = line + 1
        statement = input().strip(';')
    
    intervals.sort()
    # print(intervals)
    stack = []
    for interval in intervals:
        if -interval[1] < interval[0]:
            print('bad')
            return
        while len(stack) > 0:
            if -stack[-1][1] < interval[0]:
                # independent interval, pop it
                stack.pop()
            elif interval[0] > stack[-1][0] and stack[-1][1] > interval[1]:
                # crossing! invalid!
                print('bad')
                return
            else:
                # proper nested
                break

        stack.append(interval)
    
    print('good')

num_cases = int(input())
for _ in range(num_cases):
    solve()
