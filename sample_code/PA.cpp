#include<bits/stdc++.h>

using namespace std;

int state[11][3];
int dir[11][3];
int wb[11][3];
int tape[32];

int get_int()
{
	static char buf[16384];
	static char *p = NULL;
	int ret;
	if(p==NULL) {
		fgets(buf,sizeof(buf),stdin);
		p = strtok(buf," (),\n");
	}
	ret = atoi(p);
	p = strtok(NULL," (),\n");
	//fprintf(stderr,"token: %d\n",ret)
	return ret;
}

void solve()
{
    int n; /* n state */
	n = get_int();
    for(int i = 1; i < n; i++)
    {
        for(int j = 0; j < 3; j++)
		{
			state[i][j] = get_int();
			dir[i][j] = get_int();
			wb[i][j] = get_int();
		}
    }
    int m; /* m inputs */
	m = get_int();
    while(m--)
    {
        int head = 10, cur_state = 1; // current state: 1
        int len;
		len = get_int();
        for(int i = 0; i < 32; i++) tape[i] = 2; // fill blanks
        for(int i = 0; i < len; i++) tape[head+i] = get_int();
    
        bool halt=(n==1);
        for(int step_cnt = 0; step_cnt < 10 && !halt; step_cnt++)
        {
            //fprintf(stderr,"state: %d, head: %d, tape: %d\n", cur_state, head, tape[head]);
            //for(int i = 0; i < 32; i++) fprintf(stderr,"%d%c",tape[i],i==31?'\n':' ');
            int next_state = state[cur_state][tape[head]];
            int next_dir = dir[cur_state][tape[head]];
            tape[head]=wb[cur_state][tape[head]];
            head+=next_dir;
            cur_state=next_state;
            if(cur_state == n) halt = true;
        }
        puts(halt?"yes":"no");
    }
}

int main()
{
    int ncases;
	ncases = get_int();
    for(int i = 1; i <= ncases; i++)
    {
        printf("Machine #%d:\n",i);
        solve();
    }
    return 0;
}
