import random

def rand_trans(n):
    return (random.randint(1,n),-1+2*random.randint(0,1),random.randint(0,2))

def gen(n):
    if n > 10:
        n = 10
    print(n)
    for i in range(1,n):
        print(rand_trans(n),rand_trans(n),rand_trans(n))
    m = random.randint(80,100)
    print(m)
    for i in range(m):
        x = random.randint(5,10)
        out = [str(x)]
        for _ in range(x):
            out.append(str(random.randint(0,1)))
        print(' '.join(out))

num_cases = 20
print(num_cases)
for _ in range(num_cases):
    gen(_+1)
