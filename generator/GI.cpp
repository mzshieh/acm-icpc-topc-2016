#include<bits/stdc++.h>
using namespace std;
const int n=50000;
int main(){
	ranlux24 rng;
	
	printf("20\n");
	
	printf("%d\n",n);
	for(int i=1;i<n;i++) printf("%d ",i);
	printf("%d\n0 0\n",n);
	
	printf("%d\n",n);
	for(int i=1;i<n;i++) printf("%d ",-i);
	printf("%d\n0 0\n",-n);
	
	printf("%d\n",n);
	for(int i=1;i<=n;i++){
		printf("%d%c",i&1? -n+i : (n+i)/2,i==n?'\n':' ');
	}
	printf("2 1\n");
	
	printf("%d\n",n);
	for(int i=1;i<=n;i++){
		printf("%d%c",n,i==n?'\n':' ');
	}
	printf("100 100\n");
	
	printf("%d\n",n);
	for(int i=1;i<=n/2;i++){
		printf("%d ",i*2);
	}
	for(int i=n/2;i>=1;i--){
		printf("%d%c",i*2,i==1?'\n':' ');
	}
	printf("13 17\n");
	
	for(int T=0;T<15;T++){
		printf("%d\n",n);
		for(int i=1;i<=n;i++) printf("%d%c",rng()%(n+n+1)-n,i==n?'\n':' ');
		int a=rng()%101,b=rng()%min(a*a/4+1,101);
		printf("%d %d\n",a,b);
	}
	return 0;
}