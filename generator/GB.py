#!/usr/bin/env python3
import random

def gen():
    n = random.randint(0,3)
    print(n)
    if n == 0:
        return
    edge = set()
    for i in range(n):
        u = random.randint(1,3)
        v = random.randint(1,3)
        while u == v or (u,v) in edge:
            u = random.randint(1,3)
            v = random.randint(1,3)
        edge.add((u,v))
        edge.add((v,u))
        print(u,v)

num_cases = 100
print(num_cases)
for _ in range(num_cases):
    gen()
    gen()
