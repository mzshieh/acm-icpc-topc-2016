#!/usr/bin/env python3
import random, sys

def rand_bracket(weight):
    if weight == 2:
        return '()'
    elif weight == 3:
        return '(())'
    elif weight == 4:
        return '((()))'
    elif weight >= 105:
        return '(' * 99 + ')' + rand_bracket(weight-100) + ')' * 98
    else:
        L = random.randint(2,weight-3)
        R = weight - L - 1
        return '(' + rand_bracket(L) + rand_bracket(R) + ')'

def bad_bracket(weight):
    buf = rand_bracket(weight)
    n = len(buf)
    pos = random.randrange(n-n//5,n-1)
    while buf[pos]=='(':
        pos = random.randrange(n-n//5,n-1)
    return buf[:pos]+']'+buf[pos+1:]

def statement(brackets):
    was_left = False
    left = []
    cond = 1
    line = 0
    for _ in brackets:
        if _ == ')':
            print('if (expr_%d()) goto line_%d;' % (cond, left.pop()))
            line = line + 1
            cond = cond + 1
            was_left = False
        elif _ == ']':
            bad_line = random.choice(left)
            while bad_line == left[-1]:
                bad_line = random.choice(left)
            print('if (expr_%d()) goto line_%d;' % (cond, bad_line))
            left.pop()
            line = line + 1
            cond = cond + 1
            was_left = False
        elif was_left:
            left.append(line)
        else:
            was_left = True
            line = line + 1
            cond = cond + 1
            left.append(line)
            print('line_%d: puts("%d");' %(line,line))

def yes(n):
    statement(rand_bracket(n))
    return

def no(n):
    statement(bad_bracket(n))
    return

num_tests = int(input())
code_length = int(input())

print(num_tests)
first = True
second = False
ans = []

for _ in range(num_tests):
    if first:
        first = False
        second = True
        statement('('*(code_length-1) + ')'*(code_length-1))
        ans.append('good')
    elif second:
        second = False
        statement('('*(code_length-3) + ')'*(code_length-3))
        print('if (expr_%d()) goto line_%d;' % (code_length-1,code_length))
        print('line_%d: puts("%d");'%(code_length,code_length))
        ans.append('bad')
    elif random.randint(0,1)==0:
        yes(code_length)
        ans.append('good')
    else:
        no(code_length)
        ans.append('bad')
    print('END')

for _ in ans:
    print(_,file=sys.stderr)
