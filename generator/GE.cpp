#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> pii;
int main(){
    int T = 20;
    int n = 10000;
    int range = 1000;
    int x,y;
    srand(time(NULL));
    printf("%d\n",T);
    while(T--){
        set<pii>used;
        printf("%d\n",n);
        for(int i=0;i<n;i++){
            x=rand()%(2*range+1)-range,y=rand()%(2*range+1)-range;
            while(used.count(make_pair(x,y))){
                x=rand()%(2*range+1)-range,y=rand()%(2*range+1)-range;
            }
            used.insert(make_pair(x,y));
            printf("%d %d\n",x,y);
        }
    }
    return 0;
}
