#!/usr/bin/env python3
import random
def rp():
    x = random.choice('ABCDEFGHIJ')
    y = random.choice('ABCDEFGHIJ')
    return '( %s and %s or not %s and not %s )' % (x,y,x,y)

print(20)
for i in range(20):
    print(10)
    print(' and '.join([rp() for _ in range(7)]))
