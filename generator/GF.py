#!/usr/bin/env python3
import random, sys

def rand_4d(n):
    steps = []
    for _ in range(n):
        steps.append(random.choice(('L','R','U','D')))
    print(''.join(steps))
    
def print_lr(seq):
    dr = ('L','U','R','D')
    head = 0
    steps = []
    for _ in seq:
        head = (head + 1 + 2 * _) % 4
        steps.append(dr[head])
    print(''.join(steps))
    

dragon = [0]
for _ in range(16):
    dragon = dragon + [0] + [1-x for x in reversed(dragon)]
dragon.insert(0,0)

num_tests = int(input())
print(num_tests)
size = int(input())
for _ in range(num_tests-5):
    if _ < 10 :
        print_lr(dragon[:2**(_+2)])
    elif _ % 2 == 0:
        print_lr([random.randint(0,1) for _ in range(size)])
    else:
        rand_4d(size)
qsize = size//4
hsize = size//2
print('RU'*qsize+'LD'*qsize)
print('R'*qsize+'D'*qsize+'L'*qsize+'U'*qsize)
print('L'*(hsize-1)+'UR'+'D'*(hsize-1))
print('U'+'R'*(hsize-1)+'D'+'L'*(hsize-1))
print('DLU'+'R'*(size-6)+'ULD')
