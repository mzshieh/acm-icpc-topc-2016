#!/usr/bin/env python3
import random, sys

def gen():
    n = random.randint(1,50)
    R = random.randint(2,10)
    print(n)
    u = [str(random.randrange(R)) for _ in range(n)]
    v = [str(random.randrange(R)) for _ in range(n)]
    print(' '.join(u))
    print(' '.join(v))
    cnt = 0
    for i in range(n):
        if u[i] != v[i]:
            cnt = cnt + 1
    print(cnt,file=sys.stderr)

num_cases = 100
print(num_cases)
for _ in range(num_cases):
    gen()
