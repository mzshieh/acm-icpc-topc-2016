### Official Language 官方語言

The official language of ACM-ICPC is English. The Chinese problem descrpition is translated from English. Please refer to the English version if the Chinese statement is ambiguous to you.
本競賽的官方語言為英文。中文題面乃是翻譯英文原題。請以英文題目為準。

### Rules

Contestants will be disqualified if they violate any one of the following rules.
1. No machine-readable materials (e.g., source codes, templates, etc.) are allowed. However, paper-based materials, such as textbooks, dictionaries, printed notes, etc., are allowed.
2. Contestants are only allowed to contact his/her teammates during the contest. Contestants shall not discuss with his/her coach and other teams.
3. Contestants shall only access the internet for downloading the problem description, submitting source codes, requesting problem clarification and checking the scoreboard. Any other type of internet access is prohibited.
4. A team shall not simultaneously use more than one computer to write programs during the contest. Contestant shall not use any other type of electronic devices, except extra monitors and printers.
5. All malicious actions interfering the contest are prohibited.
The coach has to submit a photo of the team taken during the contest and declare that the team abides by the rules during the contest.

### Scoring and Ranking

+ Disqualified teams will be removed from the ranking.
+ Only C, C++, Java, Python are provided in this contest. The judge system only accepts programs which can be properly compiled and executed. A problem is solved if the submitted program terminates and outputs correctly in time. The responses of the judge system are listed as follows.
    * CE: The program cannot be properly compiled or executed.
    * TLE: The program uses too much time.
    * RE: Run-time error. The program cannot terminate normally.
    * MLE: The program uses too much memory.
    * WA: The output is incorrect.
    * AC: The program is accepted by the judge system, and the problem is solved.
+ Teams are ranked according to the most problems solved. Teams who solve the same number of problems are ranked by least total time. The total time is the sum of the time consumed for each problem solved. The time consumed for a solved problem is the time elapsed from the beginning of the contest to the submittal of the accepted run plus 20 penalty minutes for every rejected run for that problem regardless of submittal time. There is no time consumed for a problem that is not solved.
+ Breaking ties in ranking, if necessary, is according to the following order:
    * the less total number of submissions of the solved problems;
    * the shorter elapsed time of the first solved problem;
    * the shorter elapsed time of the second solved problem, and so on.

### 競賽規則

違反下列規則，將導致參賽者失去參賽資格。
1. 不得使用任何機器可讀的資料，如預先寫好存放於電腦中的程式碼。但可以使用紙本資料，如教科書、字典、筆記以及列印好的紙本程式碼。
2. 在比賽過程中，參賽者只能與隊友討論。競賽期間與教練或其他隊伍聯繫均屬違規行為。
3. 參賽者只能夠透過網路下載題目敘述、上傳解答程式碼、提問澄清疑點與查看計分板。使用網路存取其他資訊均屬違規。
4. 每個隊伍僅可使用一台電腦撰寫程式與上傳程式碼。於競賽期間除使用印表機列印題目與程式碼以及透過額外的螢幕閱讀題目之外，不得使用任何其他電子裝置。
5. 不得做出任何意圖妨礙比賽進行及影響比賽公平性的惡意行為。
賽後教練需上傳一張競賽時照片並保證該隊伍遵照規則進行比賽。

### 計分與排名

1. 違反競賽規則以致失去參賽資格者，不予計分與排名。
2. 本次競賽僅提供 C、C++、Java、Python ，不接受其他程式語言。程式需能正常執行，且在時間限制內計算完畢輸出正確答案，才算答對。以下為裁判系統常見回應：
    + CE: 語法有錯誤或其他因素以致於無法編譯或執行。
    + TLE: 未能在時間限制內執行完畢。
    + RE: 執行時錯誤，程式無法正常執行完畢。
    + MLE: 未能在記憶體限制內執行完畢。
    + WA: 在時限內執行完畢但輸出錯誤。
    + AC: 答對。
3. 隊伍以解題數量多者排名較前，解題數量相同時，以總消耗時間少者排名較前。答對的題目的消耗時間計算方式為比賽開始至解出題目所消耗的分鐘數。如解出前有答錯，每答錯一次需要另加 20 分鐘。>總消耗時間為所有答對題目的消耗時間加總。未答對的題目不計消耗時間。
4. 如果在前述計分規則無法分出勝負，則依序以下列規則決定排名：
    + 總提交次出少者排名較前
    + 最先解出的題目消耗時間較短者
    + 次先解出的題目消耗時間較短者，如再相同則類推
