import sys, os

main = open('main.tex','rt')
dummy = open('dummy.tex','wt')
prefix = main.read().split(r'\begin{document}')[0]
dummy.write(prefix)

contents = [r'\begin{document}',r'\begin{center}',r'{\LARGE Problem Z}\\',
            r'{\Large Dummy}\\',r'{Time limit: $x$ second}\\',
            '{Memory limit: 256 megabytes}',r'\end{center}',r'\end{document}','']

dummy.write('\n'.join(contents))