import sys, os

name = sys.argv[1]
if not os.path.isdir(name):
    sys.exit(0)


main = open('main.tex','rt')
each = open(name+'/'+name+'.tex','wt')
prefix = main.read().split(r'\begin{document}')[0]
prefix = '../header_'.join(prefix.split('header_'))
each.write(prefix)
each.write('\\begin{document}\n')
each.write('\\pagenumbering{gobble}\n')
each.write('\\input{'+name+'.eng.tex}\n')
each.write('\\newpage')
each.write('\\input{'+name+'.cht.tex}\n')
each.write('\\end{document}\n')
